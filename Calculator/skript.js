function getClear(){
    document.getElementById("current-value").innerText = 0
    document.getElementById("previous-value").innerText = ''
    postEqual=1
}
function getDelete(){
    let output = document.getElementById("current-value").innerText
    output= output.substring(0,output.length-1)
    if (output.length===0){output=0}
    document.getElementById("current-value").innerText=output
}

function getPreviousOperand(){
    return document.getElementById("previous-value").innerText
}
function printPreviousOperand(chislo){
    document.getElementById("previous-value").innerText += chislo
}
function getCurrentOperand(){
    return document.getElementById("current-value").innerText 
}
function printCurrentOperand(number){
    if (number === ',' && document.getElementById("current-value").innerText.includes(',')){return}
    if (number === '^' && document.getElementById("current-value").innerText ==='' | document.getElementById("current-value").innerText.includes('^')){return}
    if (postEqual=== 1){ 
    document.getElementById("current-value").innerText = ''
    document.getElementById("previous-value").innerText = ''
    postEqual=0}
    document.getElementById("current-value").innerText += number
}
document.getElementById("current-value").innerText = 0
function updateCurrentOperand(operation){
    if (postEqual=== 1){document.getElementById("previous-value").innerText = ''; postEqual=0}
    if(operation=== '(' && getCurrentOperand()=== '0' ){  document.getElementById("current-value").innerText = ''}
    printPreviousOperand (getCurrentOperand()+operation)
    document.getElementById("current-value").innerText =''
}

var postEqual=1
function equal(){
    printPreviousOperand(getCurrentOperand())
    let virajeniye = getPreviousOperand()
    document.getElementById("current-value").innerText = ''
    let splitVirajeniye = splitString(virajeniye)
    let otvet = Calculator(splitVirajeniye)
    printCurrentOperand(otvet)
    postEqual=1
} 
function splitString(term) {
    let splitStringList = []
    let counterNum = -1
    for (let i = 0; i < term.length; i++)
    {
        let temp = true
        temp = TryParseInt(term[i],false)
        if (temp !== false)
        {
            if (counterNum === -1) { counterNum = 0; }
            counterNum *= 10
            counterNum += parseInt(term[i])
            if (i === term.length - 1)
            {
                splitStringList[splitStringList.length] = counterNum
            }
        }
        else
        {  
             if (counterNum !== -1) { splitStringList[splitStringList.length] = counterNum}
             if (counterNum !== -1 && term[i].toString() ==='(' ){ splitStringList[splitStringList.length]='×' }
        counterNum = -1
        splitStringList[splitStringList.length]= term[i]
        }
    }
    return splitStringList
}
function TryParseInt(str,defaultValue) {
    var retValue = defaultValue;
    if(str !== null) {
        if(str.length > 0) {
            if (!isNaN(str)) {
                retValue = parseInt(str);
            }
        }
    }
    return retValue;
}


var numsStack = []
var symbolPriorityStack = []
var symbolStack = []
var priority=0 , chekPriority = 0, codeZero=1

function SetStucks(simbol)
 {
    chekPriority = priority
    symbolPriorityStack.push(priority)
    symbolStack.push(simbol)
}

function doCalculation(stope)
{
    let result = 0
    let d = 0
    let c = numsStack.pop();
    if (numsStack.length !== 0 && numsStack.length !== stope) { d = numsStack.pop(); }
    switch (symbolStack.pop())
    {
         case '+':
             result = d + c;
             break;
         case '-':
             result = d - c;
             break;
         case '.':
             while (c >= 1){ c /= 10; }
             result = d + c;
             break;
         case '×':
             result = d * c;
             break;
         case '÷':
             if (c === 0) { alert("На 0 делить нельзя"); codeZero = 0; break; }
             result = d / c;
             break;
         case '^':
             result = Math.pow(d,c)
             break;
    }
    symbolPriorityStack.pop()
     numsStack.push(result)
     if (symbolPriorityStack.length !== 0) { chekPriority = symbolPriorityStack[symbolPriorityStack.length -1] }
     else { chekPriority = 0 }
     return result
     
 }
function Calculator(splitVirajeniye)
{
    let stop = -1
    let temp = true
    for (let i = 0; i < splitVirajeniye.length; i++)
    {
            temp = TryParseInt(splitVirajeniye[i].toString(),false)
        if (temp !== false)
        {
            numsStack.push(splitVirajeniye[i])
            if (i === splitVirajeniye.length - 1 && symbolPriorityStack.length !== 0)
            {
                do
                {
                    doCalculation(stop)
                    if (codeZero === 0) { break; }
                } while (symbolStack.length !== 0)
                break;
            }
        }
        else
        {
            switch (splitVirajeniye[i].toString())
            {
                case '+':
                case '-':
                    priority = 1;
                    break;
                case '×':
                case '÷':
                    priority = 2;
                    break;
                case '^':
                    priority = 3;
                    break;
                case '.':
                    priority = 4;
                    break;
                case '(':
                    stop = numsStack.length;
                    symbolPriorityStack.push(0)
                    chekPriority = 0
                    symbolStack.push(splitVirajeniye[i]);
                    break;
                case ')':
                    while (symbolStack[symbolStack.length - 1] !== '(')
                    {
                         doCalculation(stop);
                        if (codeZero === 0) { break; }
                    }
                    if (symbolStack[symbolStack.length - 1] === '(') { 
                        symbolStack.pop(); 
                        symbolPriorityStack.pop();
                        stop = -1;
                        chekPriority = 0;
                        if (symbolPriorityStack.length !== 0) { chekPriority = symbolPriorityStack[symbolPriorityStack.length - 1 ] }
                    }
                    if (i === splitVirajeniye.length - 1 && symbolPriorityStack.length !==0)
                    {
                        do
                        {
                            doCalculation(stop);
                            if (codeZero === 0) { break; }
                        }
                        while (symbolStack.length !== 0);
                        break;
                    }
                    break;
            }
            if (splitVirajeniye[i].toString() !== '(' && splitVirajeniye[i].toString() !== ')') {
                while (true)
                {
                    if (priority > chekPriority)
                    {
                        SetStucks(splitVirajeniye[i].toString());
                        break;
                    }
                    else if (priority <= chekPriority)
                    {
                            doCalculation(stop);
                            if (codeZero === 0) { break; }
                        
                    }
                }
            }
        }
        if (codeZero === 0){break;}
    }
    return numsStack.pop();
}
 var buttonSound = new Audio();
 buttonSound.src = "TinyButtonPush.mpeg"
        



 



